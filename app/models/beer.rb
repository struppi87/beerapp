class Beer < ActiveRecord::Base
  belongs_to :user
    default_scope -> { order(created_at: :desc) }
    mount_uploader :picture, PictureUploader
    validates :user_id, presence: true
    validates :beername, presence: true, length: { maximum: 50 }
    validates :brewery, presence: true, length: { maximum: 50 }
    validates :beertype, presence: true, length: { maximum: 50 }
    validates :alcohol, presence: true, numericality: { less_than_or_equal_to: 15 }
    validates :description, presence: true, length: { maximum: 140 }
    
    private
    
    # Validates the size of an uploaded picture.
    def picture_size
      if picture.size > 5.megabytes
          errors.add(:picture, "Das Bild sollte nicht größer als 5MB sein.")
      end
    end
end
