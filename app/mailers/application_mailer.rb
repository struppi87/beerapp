class ApplicationMailer < ActionMailer::Base
    default from: "noreply@aufeinbierchen.de"
  layout 'mailer'
end
