class BeersController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def create
    @beer = current_user.beers.build(beer_params)

    if @beer.save
      flash[:success] = "Bier wurde hinzugefügt"
      @user = current_user
    else
      @feed_items = []
    end

    render 'static_pages/home'
  end

  def show
    @beer = Beer.find(params[:id])
  end

  def index
    @beers = Beer.paginate(page: params[:page])
    @newest = Beer.order("created_at desc").limit(12)
    @top5 = Beer.order("rating desc").limit(5)
  end

  def destroy
    @beer.destroy
    flash[:success] = "Bier wurde gelöscht"
    render 'static_pages/home'
  end

  private

  def beer_params
    params
      .require(:beer)
      .permit(
        :beername,
        :brewery,
        :beertype,
        :alcohol,
        :rating,
        :description,
        :picture
      )
  end

  def correct_user
    @beer = current_user.beers.find_by(id: params[:id])
    redirect_to root_url if @beer.nil?
  end
end
