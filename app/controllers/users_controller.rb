class UsersController < ApplicationController
before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
before_action :correct_user,   only: [:edit, :update]
before_action :admin_user,     only: :destroy
    
    def index
        @users = User.paginate(page: params[:page])
    end
    
  def new
      @user = User.new
  end
    def show
        @user = User.find(params[:id])
        @beers = @user.beers.paginate(page: params[:page])
    end
    def create
    @user = User.new(user_params)
            if @user.save
            @user.send_activation_email
            flash[:info] = "Wir haben Ihnen einen Aktivierungslink an die hinterlegte E-Mail-Adresse gesendet."
      redirect_to root_url
      # Handle a successful save.
            else
                render 'new'
            end
    end
    
    def edit
    @user = User.find(params[:id])
  end
   
    
     def destroy
    User.find(params[:id]).destroy
         flash[:success] = "Das Profil wurde gelöscht"
    redirect_to users_url
  end
    
    def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
        flash[:success] = "Profil wurde aktualisiert"
      redirect_to @user
    else
      render 'edit'
    end
  end
    
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
    
    # Before filters

    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?
        store_location
          flash[:danger] = "Bitte loggen Sie sich ein."
        redirect_to login_url
      end
    end
    
    # Confirms the correct user.
    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end
    
    # Confirms an admin user.
    def admin_user
      redirect_to(root_url) unless current_user.admin?
    end
end