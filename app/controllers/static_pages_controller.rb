class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @beer = current_user.beers.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @newest = Beer.order("created_at desc").limit(6)
      @top5 = Beer.order("rating desc").limit(5)
    end
  end

  def beers
    if logged_in?
      @beer = current_user.beers.build
      @newest = Beer.order("created_at desc").limit(12)
      @top5 = Beer.order("rating desc").limit(5)
    end
  end

  def help
  end

  def about
  end

  def contact
  end
end
