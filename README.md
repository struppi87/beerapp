# Auf ein Bierchen

## System dependencies

* PostgreSQL is required

## Configuration

## Database initialization

* Run `rake db:setup`, this will create the database, load the schema
and seed it with some data to test

* A user to test stuff with is:
```
email: bubu@baba.com
password: foobar
```

## Testing (tbd)

## Deploy (tbd)
