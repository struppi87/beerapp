require 'test_helper'

class BeersControllerTest < ActionController::TestCase
    
def setup
    @beer = beers(:one)
  end

  test "should redirect create when not logged in" do
      assert_no_difference 'Beer.count' do
          post :create, beer: { beername: "test" }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
      assert_no_difference 'Beer.count' do
          delete :destroy, id: @beer
    end
    assert_redirected_to login_url
  end
    
    test "should redirect destroy for wrong beer" do
    log_in_as(users(:michael))
        beer = beers(:one)
        assert_no_difference 'Beer.count' do
            delete :destroy, id: beer
    end
    assert_redirected_to root_url
  end

end