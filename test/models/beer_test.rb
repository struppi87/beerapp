require 'test_helper'

class BeerTest < ActiveSupport::TestCase
  def setup
    @user = users(:michael)
    @beer = @user.beers.build(beername: "test", brewery: "test", beertype: "test", alcohol: 5, rating: 5,  description: "test", user_id: @user.id)
  end

test "should be valid" do
      assert @beer.valid?
  end

test "user id should be present" do
      @beer.user_id = nil
      assert_not @beer.valid?
  end
    
test "beername should be present" do
        @beer.beername = "   "
        assert_not @beer.valid?
  end

test "beername should be at most 50 characters" do
    @beer.beername = "a" * 51
        assert_not @beer.valid?
  end
    
test "brewery should be present" do
        @beer.brewery = "   "
        assert_not @beer.valid?
  end

test "brewery should be at most 50 characters" do
        @beer.brewery = "a" * 51
        assert_not @beer.valid?
  end
    
test "beertype should be present" do
        @beer.beertype = "   "
        assert_not @beer.valid?
  end

test "beertype should be at most 140 characters" do
        @beer.beertype = "a" * 141
        assert_not @beer.valid?
  end
    
test "alcohol should be present" do
        @beer.alcohol = "   "
        assert_not @beer.valid?
  end

    test "alcohol should be at most 15" do
        @beer.alcohol = 16
        assert_not @beer.valid?
  end

test "description should be present" do
    @beer.description = "   "
        assert_not @beer.valid?
  end

test "description should be at most 140 characters" do
    @beer.description = "a" * 141
        assert_not @beer.valid?
  end



test "order should be most recent first" do
    assert_equal beers(:most_recent), Beer.first
  end

end
