require 'test_helper'

class BeersInterfaceTest < ActionDispatch::IntegrationTest

    def setup
        @user = users(:michael)
    end

    test "beer submission (success)" do
        log_in_as(@user)
        get root_path

        assert_no_difference 'Beer.count' do
            post beers_path, beer: { beername:"" }
        end
    end

    test "beer submission (failure)" do
        log_in_as(@user)
        get root_path

        beername = "Hell"
        brewery = "Öttinger"
        beertype = "Helles"
        alcohol = 5
        description = "This beer is shit"

        assert_difference 'Beer.count', 1 do
          post beers_path, beer: {
              beername: beername,
              brewery: brewery,
              beertype: beertype,
              alcohol: alcohol,
              description: description
          }
        end

        beer = Beer.where(beername: beername).first
        assert_equal beers_path, path
        assert_match beername, response.body
    end

    test "beer deletion" do
        log_in_as(@user)
        get users_path(@user)

        assert_select 'a', text: 'löschen'
        first_beer = @user.beers.paginate(page: 1).first
        assert_difference 'Beer.count', -1 do
            delete beer_path(first_beer)
        end
    end

    test "visit a different user" do
        log_in_as(@user)
        get root_path

        get user_path(users(:lana))
        assert_select 'a', text: 'delete', count: 0
    end
end
