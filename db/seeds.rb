User.create!(name:  "Example User",
             email: "bubu@baba.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin:     true,
             activated: true,
             activated_at: Time.zone.now)

99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
              email: email,
              password:              password,
              password_confirmation: password,
              activated: true,
              activated_at: Time.zone.now)
end

users = User.order(:created_at).take(6)
50.times do
    beername = Faker::Lorem.characters(10)
    brewery = Faker::Lorem.characters(10)
    beertype = Faker::Lorem.characters(10)
    alcohol = Faker::Number.number(1)
    description = Faker::Lorem.words(10)
    users.each { |user| user.beers.create!(beername: beername, brewery: brewery, beertype:beertype, alcohol:alcohol, description:description) }
end


